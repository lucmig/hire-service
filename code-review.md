general
-------
- do not use console.log, add a logger lib which so the output can be directed according to environment and have log levels to be able to filter out debug messages.

- turn on linter

- use consistent naming either camel case or '-', don't mix; "hireableItem-dao". My preference is not to use capitals in file names.

- put all source in src/ folder.

- issue item ids on the server for better control

- param validation

package.json
------------
- dev dependencies in dependencies

  "eslint": "^3.19.0",

  "eslint-config-airbnb": "^15.0.1",


server.js
---------
- seperate the http server / routes / controller logic into seperate files

- make routes more 'restful'

  /bookings
  
  .. / GET => get all bookings

  .. /{id} GET => get a booking by id

  .. / POST => create a booking 

- Even with the production guard, the route '/johnny-dev/init/' or '/init/items/' should not be shipped. Create scripts to populate / teardown database and link to npm scripts.

- GET '/' does nothing useful, even if just a placeholder should remove until there is something useful to serve up, docs maybe?

- GET '/items' make this more 'restful' e.g. put it as GET '/hires'. Make the urls behave consistently and as expected when calling verbs on a route, see [this useful best practices blog](https://blog.mwaysolutions.com/2014/06/05/10-best-practices-for-better-restful-api/)

- POST '/admin/items' make this more 'restful' e.g. put it as POST '/hires'. see above

- POST '/booking' inconsistent, make this '/bookings'

- POST '/hire' is an update on hires so should be PUT '/hires'

- setItemStatusToAvailable is unsafe, item can change between being updated and the booking deleted, needs a transaction maybe look at level-transactions lib.

- PUT '/hire' this is an update in a hire (returned), so either merge this code with update to checkout item and add a param path would be PUT '/hires'. Or split as e.g. PUT '/hires/checkout' & '/hires/return'

- hire.cancel unsafe, see about transactions above

- DELETE '/hire/' should be DELETE '/hires'


persistence / index.js
----------------------

-  dropAll method: Do not have debug / test code in code that is deployed.

persistence / booking-dao.js
----------------------------

- save & update unsafe, see notes above about transactions

persistence / hireableItem-dao.js
---------------------------------

- save & update unsafe, see notes above about transactions

- these methods are very simmilar to booking dao, see how common code could be factored out.