import Router from 'koa-router';
import itemsRouter from './items/router';

export default new class ApiRouter {
  constructor() {
    this.router = new Router();

    this.router
      .use('/items', itemsRouter.routes());
  }
}().router;
