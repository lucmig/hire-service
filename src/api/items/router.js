import Router from 'koa-router';
import items from './';

export default new class LocationRouter {
  constructor() {
    this._router = new Router();

    this._router
    /**
     * @api {get} / Get all items
     * @apiGroup items
     * @apiSampleRequest /api/items
     *
     * @apiSuccess (200) {Array} all items.
     */
      .get('/', this.getItems())

    /**
     * @api {post} / Add an item
     * @apiGroup items
     * @apiSampleRequest /api/items
     *
     * @apiSuccess (200) {String} new item id.
     */
      .post('/', this.addItem());
  }

  get router() {
    return this._router;
  }

  getItems() {
    return async (ctx) => {
      try {
        const res = await items.all();
        ctx.body = res;
      }
      catch (err) {
        ctx.error = err;
        ctx.status = 400;
        ctx.body = err;
      }
    };
  }

  addItem() {
    return async (ctx) => {
      try {
        const res = await items.add(ctx.body);
        ctx.body = res;
      }
      catch (err) {
        ctx.error = err;
        ctx.status = 400;
        ctx.body = err;
      }
    };
  }
}().router;
