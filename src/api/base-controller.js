import uuid from 'uuid/v4';
import logger from '../logger';
import db from '../data';

export default class BaseController {
  constructor(key) {
    this.key = key;
  }

  async add(item) {
    try {
      const valItem = this.validate(item);
      const id = uuid();
      await new Promise(
        (resolve, reject) => {
          db.put(`${this.key}:${id}`, valItem, (err) => {
            if (err) reject(err);
            resolve();
          });
        });

      return id;
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }

  async all() {
    try {
      const items = await new Promise(
        (resolve, reject) => {
          // can this be streamed directly to response?
          const result = [];
          db.createReadStream({ start: `${this.key}:`, end: `${this.key}:Z` })
          .on('data', (data) => {
            logger.debug(`${data.key} =  ${JSON.stringify(data.value)}`);
            result.push(data);
          })
          .on('error', (err) => {
            reject(err);
          })
          .on('close', () => {
            logger.debug('Stream closed');
          })
          .on('end', () => {
            logger.debug('Stream ended ', result);
            resolve(result);
          });
        });
      return items;
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }

  async getById(id) {
    try {
      // TODO
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }

  async update(item) {
    try {
      // TODO
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }

  async delete(id) {
    try {
      // TODO
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }

  validate(item) {
    return item;
  }
}
