import koa from 'koa';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';
import mount from 'koa-mount';
// import favicon from 'koa-favicon';
import routesConf from './router';

export default new class Koa {
  constructor() {
    const app = this.app = new koa();
    // app.keys = config.keys;
    app.use(bodyParser());

    // routes
    app.use(routesConf.router.routes());

    // docs
    app.use(mount('/', serve(`${__dirname}/docs`)));
    app.use(mount('/docs', serve(`${__dirname}/docs`)));

    // coverage
    app.use(mount('/', serve(`${__dirname}/test-cover/lcov-report`)));
    app.use(mount('/cover', serve(`${__dirname}/test-cover/lcov-report`)));

    // code analysis
    app.use(mount('/', serve(`${__dirname}/analysis`)));
    app.use(mount('/analysis', serve(`${__dirname}/analysis`)));

    // favicon
    // app.use(favicon(`${__dirname}/favicon.ico`));

    // CORS
    app.use(async (ctx, next) => {
      ctx.set('Access-Control-Allow-Origin', '*');
      ctx.set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
      ctx.set('Access-Control-Allow-Headers', 'accept, Content-Type, Authorization, Content-Length, X-Requested-With');
      if (ctx.request.method === 'OPTIONS') {
        ctx.status = 200;
      }
      else {
        await next();
      }
    });
  }
}();

