import server from './server';
import logger from './logger';

(async () => {
  try {
    await server.init();
    server.start();
  }
  catch (err) {
    logger.error(err);
  }
})();
