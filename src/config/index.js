import _ from 'lodash';

const common = {
  env: process.env.NODE_ENV || 'development'
};

const envConfig = require('./' + common.env) || {};

export default _.merge(common, envConfig.default);
