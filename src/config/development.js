export default {
  ip: process.env.IP || 'localhost',
  port: process.env.PORT || 8888
};
