import level from 'level';
import sublevel from 'sublevel';

export default new class Db {
  constructor() {
    this.db = sublevel(
      level('hire-service.db', { valueEncoding: 'json' }));
  }
}().db;

