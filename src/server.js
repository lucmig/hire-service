import http from 'http';
import logger from './logger';
import koaConf from './koa';
import config from './config';

export default new class Server {

  init() {
    return new Promise(
      (resolve, reject) => {
        try {
          this.httpServer = http.createServer(koaConf.app.callback());
          resolve();
        }
        catch (err) {
          logger.error(err);
          reject(err);
        }
      });
  }

  start() {
    this.httpServer.listen(config.port);
    logger.debug(`listening on http port ${ config.port }`);
    logger.debug(`deployment environment is ${ config.env }`);
  }

  stop() {
    if (this.httpServer) this.httpServer.close();
    logger.debug('stopped');
  }
}();
