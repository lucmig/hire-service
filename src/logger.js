import winston from 'winston';
import path from 'path';

export default new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      level: 'debug',
      json: false,
      timestamp: false,
      colorize: 'all'
    }),
    new (winston.transports.File)({
      filename: path.join(__dirname, '/server-debug.log'),
      json: false
    })
  ],
  exceptionHandlers: [
    new (winston.transports.Console)({
      json: false,
      timestamp: true }),
    new winston.transports.File({
      filename: path.join(__dirname, '/server-exceptions.log'),
      json: false })
  ],
  exitOnError: false
});
