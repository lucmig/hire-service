import Router from 'koa-router';
import readPkg from 'read-pkg';
import apiRouter from './api/router';

export default new class Routes {
  constructor() {
    const router = new Router();
    router
      // api routes
      .use('/api', apiRouter.routes())

      // base routes
      /**
       * @api {get} /version Version
       * @apiGroup General
       * @apiSampleRequest /version
       *
       * @apiSuccess {String} version Deployment version
       */
      .get('/version', this.getVersion());
    this.router = router;
  }

  getVersion() {
    return async (ctx) => {
      const pkg = await readPkg();
      ctx.body = `v${pkg.version}`;
    };
  }
}();
